# Output

```
Adding group: all
Adding group: func_foo
Adding test: tc_1
Adding test: tc_2
Adding test: tc_3
Adding test: tc_4
Adding group: func_bar
Adding test: tc_5
Adding test: tc_6
Adding group: func_bar_1
Adding test: tc_7
Adding group: func_bar_2
Adding test: tc_8
Checking group: ROOT
Checking group: all
Checking group: func_foo
Checking group: func_bar
Checking group: func_bar_1
Checking group: func_bar_2
Checking test: tc_1
Checking test: tc_2
Checking test: tc_3
Checking test: tc_4
Checking test: tc_5
Checking test: tc_6
Checking test: tc_7
Checking test: tc_8
Building test: tc_1
Building test: tc_2
Building test: tc_3
Building test: tc_4
Building test: tc_5
Building test: tc_6
Building test: tc_7
Building test: tc_8
Running test: tc_2
Running test: tc_3
Running test: tc_4
Running test: tc_5
Running test: tc_8

SUMMERY
***************************************************************************
Name        Build       Run         Build-time  Run-time    
---------------------------------------------------------------------------
tc_1        FAILED      -           0.4         0.0         
tc_2        PASSED      PASSED      0.5         0.86        
tc_3        PASSED      PASSED      0.7         0.64        
tc_4        PASSED      PASSED      0.47        0.38        
tc_5        PASSED      PASSED      0.28        0.12        
tc_6        FAILED      -           0.12        0.0         
tc_7        FAILED      -           0.36        0.0         
tc_8        PASSED      FAILED      0.6         0.71        

TESTS
***************************************************************************
Name        parent      procedure   file        setup       timeout     active      
---------------------------------------------------------------------------
tc_1        func_foo    automatic   tc_1.c      setup_1.xml 60          1           
tc_2        func_foo    automatic   tc_2.c      setup_1.xml 60          0           
tc_3        func_foo    automatic   tc_3.c      setup_1.xml 60          1           
tc_4        func_foo    automatic   tc_4.c      setup_1.xml 300         0           
tc_5        func_bar    manual      tc_5.c      setup_2.xml 60          0           
tc_6        func_bar    manual      tc_6.c      setup_2.xml 60          0           
tc_7        func_bar_1  inspection  tc_7.c      setup_2.xml 30          0           
tc_8        func_bar_2  automatic   tc_8.c      setup_2.xml 400         1           

GROUPS
***************************************************************************
Name        parent      procedure   file        setup       timeout     active      
---------------------------------------------------------------------------
ROOT        ROOT                                            0           0           
all         ROOT        automatic                           60          0           
func_foo    all         automatic                           60          0           
func_bar    all         manual                              60          0           
func_bar_1  func_bar    inspection                          30          0           
func_bar_2  func_bar    automatic                           400         1           

Process finished with exit code 0
```