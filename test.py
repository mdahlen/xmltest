import xml.etree.ElementTree as xml
import time
import random


# ****************************************
class TestCase:
    def __init__(self, name: str):
        self.name = name
        self.procedure = str()
        self.file = str()
        self.setup = str()
        self.timeout = int()

        self.parent = str()
        self.active = bool()

        self.build_result = str()
        self.build_time = float()
        self.build_log = bytes()

        self.run_result = str()
        self.run_time = float()
        self.run_log = bytes()


# ****************************************
class TestGroup:
    def __init__(self, name: str):
        self.name = name
        self.procedure = str()
        self.file = str()
        self.setup = str()
        self.timeout = int()

        self.parent = str()
        self.active = bool()


# ****************************************
class TestSuite:
    def __init__(self, name: str, data: xml.ElementTree, query: str):
        self.name = name
        self.data = data
        self.query = query.split()

        self.groups = dict()
        self.tests = dict()

        self.groups['ROOT'] = TestGroup('ROOT')
        self.groups['ROOT'].parent = 'ROOT'

        # Iterating over direct children from 'data' and add them.
        for element in self.data.findall('./'):
            if element.tag == 'group':
                self.add_group(element, 'ROOT')
            elif element.tag == 'test':
                self.add_test(element, 'ROOT')

        for key, value in self.groups.items():
            print('Checking group:', key)
            if key in self.query or self.groups[value.parent].active:
                self.groups[key].active = True

        for key, value in self.tests.items():
            print('Checking test:', key)
            if key in self.query or self.groups[value.parent].active:
                self.tests[key].active = True

    # ********************
    def add_test(self, test: xml.Element, parent: str):
        name = test.get('name')
        if name not in self.tests.keys():
            print('Adding test:', name)
            self.tests[name] = TestCase(name)
            self.tests[name].procedure = test.get('procedure', default=self.groups[parent].procedure)
            self.tests[name].file = test.get('file', default=self.groups[parent].file)
            self.tests[name].setup = test.get('setup', default=self.groups[parent].setup)
            self.tests[name].timeout = test.get('timeout', default=self.groups[parent].timeout)
            self.tests[name].parent = parent

    # ********************
    def add_group(self, group: xml.Element, parent: str):
        name = group.get('name')
        if name not in self.groups.keys():
            print('Adding group:', name)
            self.groups[name] = TestGroup(name)
            self.groups[name].procedure = group.get('procedure', default=self.groups[parent].procedure)
            self.groups[name].file = group.get('file', default=self.groups[parent].file)
            self.groups[name].setup = group.get('setup', default=self.groups[parent].setup)
            self.groups[name].timeout = group.get('timeout', default=self.groups[parent].timeout)
            self.groups[name].parent = parent

            # loop over all sub-elements and add all groups and tests
            # since their parent element was added.
            for element in group.iter():
                if element.tag == 'group':
                    self.add_group(element, name)
                elif element.tag == 'test':
                    self.add_test(element, name)


RandomResult = ['PASSED', 'PASSED', 'PASSED', 'FAILED']


# ****************************************
def main():
    # options from command line (optparse)
    option_suite = 'big hooters'
    option_build = True
    option_run = True
    option_query = 'tc_1 tc_3 func_bar_2'

    test_to_run = xml.parse(source="test_to_run.xml")

    suite = TestSuite(name=option_suite,
                      data=test_to_run,
                      query=option_query)

    # Build tests
    if option_build:
        for (key, value) in suite.tests.items():  # type: str, TestCase
            print("Building test:", key)

            start_time = time.time()
            time.sleep(random.random())  # simulating building test with make
            suite.tests[key].build_result = random.choice(RandomResult)
            suite.tests[key].build_time = time.time() - start_time

    # Run tests
    if option_run:
        for (key, value) in suite.tests.items():  # type: str, TestCase
            if value.build_result == 'PASSED':
                print("Running test:", key)

                start_time = time.time()
                time.sleep(random.random())  # simulating running test with probe script
                suite.tests[key].run_result = random.choice(RandomResult)
                suite.tests[key].run_time = time.time() - start_time
            else:
                suite.tests[key].run_result = '-'

    # Print summary
    summary_width = 75

    columns = '{:<12}{:<12}{:<12}{:<12}{:<12}'
    print('\nSUMMERY')
    print(summary_width * '*')
    print(columns.format('Name', 'Build', 'Run', 'Build-time', 'Run-time'))
    print(summary_width * '-')
    for (key, value) in suite.tests.items():  # type: str, TestCase
        print(columns.format(value.name, value.build_result, value.run_result, round(value.build_time, 2), round(value.run_time, 2)))

    columns = '{:<12}{:<12}{:<12}{:<12}{:<12}{:<12}{:<12}'
    print('\nTESTS')
    print(summary_width * '*')
    print(columns.format('Name', 'parent', 'procedure', 'file', 'setup', 'timeout', 'active'))
    print(summary_width * '-')
    for key, value in suite.tests.items():
        print(columns.format(value.name, value.parent, value.procedure, value.file, value.setup, value.timeout, value.active))

    columns = '{:<12}{:<12}{:<12}{:<12}{:<12}{:<12}{:<12}'
    print('\nGROUPS')
    print(summary_width * '*')
    print(columns.format('Name', 'parent', 'procedure', 'file', 'setup', 'timeout', 'active'))
    print(summary_width * '-')
    for key, value in suite.groups.items():
        print(columns.format(value.name, value.parent, value.procedure, value.file, value.setup, value.timeout, value.active))


# ****************************************
if __name__ == "__main__":
    main()
